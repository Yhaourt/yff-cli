#!/bin/bash

yff_repo_url="git@gitlab.com:Yhaourt/yff.git"

if [ "$1" == "help" ] || [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
  help
  exit 0
fi

if [ "$1" == "create" ]; then
  if [ -z "$2" ]; then
    echo "Usage: $0 create <name>"
    exit 1
  fi

  # Cloning Yff
  echo "Creating new Yff project '$2' from '$yff_repo_url'..."
  git clone "$yff_repo_url" "$2"
  cd "$2" || { help; exit 1; }

  # Configuring project
  echo "Configuring '$2'..."
  rm -rf .git
  find . -type f -exec sed -i "s/yff/$2/g" {} +

  # Project created
  echo "Project '$2' created. Enjoy !"
fi

help() {
  echo "Yff commands are :"
  echo ""
  echo "  $0 create <name>   Crate a new Yff project"
}
