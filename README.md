# yff-cli

This is a CLI tool to create a new Yff Flutter project.

## How to use

### Prerequisites

- Flutter SDK && Dart SDK configured
- Android Studio && Android SDK configured
- Git installed

### Steps

1. Clone this repository
2. Run `./yff-cli.sh create <project-name>`
3. Run `flutter pub get` inside the project folder
4. Run `flutter run` to start the project
5. Enjoy!



